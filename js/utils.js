      // 以对象形式返回url后方全部的请求参数
      getRequest(search) {
              let url = search ? search : window.location.search,
                  theRequest = {};
              if (url.indexOf("?") != -1) {
                  let str = url.substr(1);
                  let strs = str.split("&");
                  for (let i = strs.length; i--;) {
                      theRequest[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]); //对字符串解码
                  }
              }
              return theRequest;
          },
          // 获取url路径中的指定参数，variable->参数键名
          getQueryVariable: (variable) => {
              let query = window.location.search.substring(1);
              let vars = query.split("&");
              for (let i = vars.length; i--;) {
                  let pair = vars[i].split("=");
                  if (pair[0] == variable) {
                      return pair[1];
                  }
              }
              return false;
          },
          // 更换url参数中的值，url->window.location.href
          changeUrlArg(url, arg, arg_val) {
              url = url ? url : window.location.href;
              let pattern = arg + '=([^&]*)';
              let replaceText = arg + '=' + arg_val;
              if (url.match(pattern)) {
                  let tmp = '/(' + arg + '=)([^&]*)/gi';
                  tmp = url.replace(eval(tmp), replaceText);
                  return tmp;
              } else {
                  if (url.match('[\?]')) {
                      return url + '&' + replaceText;
                  } else {
                      return url + '?' + replaceText;
                  }
              }
              // return url + '\n' + arg + '\n' + arg_val;
          },
          // 路径参数自动拼接
          urlArgContact(url, jump_params) {
              const URL_FLAG =
                  url.indexOf("?") == -1;
              const keys_arr = Object.keys(jump_params);
              const keys_length = keys_arr.length;
              let accType;
              if (!Boolean(jump_params)) {
                  return url;
              }
              for (let i = keys_length; i--;) {
                  const router_key = [keys_arr[i]];
                  const router_vla = jump_params[router_key];
                  URL_FLAG ? accType = i == keys_length - 1 ? '?' : '&' : accType = '&';
                  url = `${url + accType + router_key + '=' + router_vla}`;
              }
              return url
          },
          const utils = {
              /* jQuery简单动画封装 */
              jq: {
                  //放大缩小 
                  imgScale(name) {
                      $(name).hover(
                          function () {
                              $(this).css({
                                  "overflow": "hidden",
                                  "cursor": "pointer"
                              });
                              $(this)
                                  .children("img")
                                  .css({
                                      transform: "scale(1.1)"
                                  });
                              // console.log($(this).children("img").style);
                          },
                          function () {
                              $(this).css({
                                  overflow: "hidden"
                              });
                              $(this)
                                  .children("img")
                                  .css({
                                      transform: "scale(1)"
                                  });
                          }
                      );
                  },
                  //案例阴影shadow
                  caseShadow(name) {
                      $(name).hover(function () {
                          $(this).css({
                              "box-shadow": "5px 5px 10px 2px rgba(0, 0, 0, 0.1)",
                              // "box-shadow": "0px 2px 2px 0px rgba(0, 0, 0, 0.1);",
                              "transition": "box-shadow .4s ease-in-out"
                          })
                      }, function () {
                          $(this).css({
                              "box-shadow": "none",
                              "transition": "box-shadow .4s ease-in-out"
                          })
                      })
                  },
                  //文字缓动
                  textShake(name, shakeHeight) {
                      $(window).scroll(() => {
                          let scrollTop = $(window).scrollTop();
                          if (!Boolean($(name).offset())) return;
                          let top = $(name).offset().top;
                          let wh = $(window).height();
                          // console.log("元素距离顶部", top);
                          // console.log("可视高度", wh);
                          // console.log("滚动条", scrollTop);
                          // console.log("滚动条+可视高度", scrollTop + wh);
                          if (top >= scrollTop && top < scrollTop + (wh / 2 - 150)) {
                              $(name).css({
                                  transition: "all .8s ease-in-out",
                                  top: shakeHeight
                              });
                          }
                      });
                  },
                  //按钮渐变->selCssObj选中的样式,outCssObjP：离开的样式
                  btnColChange(name, selCssObj, outCssObj) {
                      $(name).hover(
                          () => {
                              console.log('name', name);
                              $(name).css(selCssObj);
                          },
                          () => {
                              $(name).css(outCssObj);
                          }
                      );
                  },
                  //回到顶部
                  scrollBackTop(speed) {
                      var targetOffset = $("body").offset().top;
                      $("html ,body").stop().animate({
                          scrollTop: targetOffset
                      }, speed);
                  }
              },
              /* 数据处理相关 */
              dataFuncs: {
                  // 若对象为null或者undefined或者''时候，转化为自定义字符串或者直接删除
                  replaceNull: function (obj, defaultData = '', del = false) {
                      del ? true : false
                      if (typeof obj === 'object') {
                          Object.keys(obj).forEach(element => {
                              let value = obj[element];
                              if (value === null || value === undefined || value === '') {
                                  if (!del) {
                                      obj[element] = defaultData; // 页面显示默认值
                                  } else {
                                      delete obj[element];
                                  }
                              } else if (typeof value === 'object') {
                                  utils.dataFuncs.replaceNull(value);
                              }
                          });
                      }
                      return obj;
                  },
                  //ES6—Reflect方式实现深拷贝
                  deepClone(obj) {
                      function isObject(exp) {
                          return Object.prototype.toString.call(exp) == '[object Object]'
                      }
                      if (!isObject(obj)) {
                          throw new Error('obj 不是一个对象！')
                      }
                      let isArray = Array.isArray(obj)
                      let cloneObj = isArray ? [...obj] : {
                          ...obj
                      }
                      Reflect.ownKeys(cloneObj).forEach(key => {
                          cloneObj[key] = isObject(obj[key]) ? utils.handleObjFuncs.deepClone(obj[key]) : obj[key]
                      })
                      return cloneObj
                  }
              },
              /* 获取时间相关->待优化 */
              timeFuncs: {
                  //获取时间，type时间粒度默认为：seconds，nextYear默认为一年
                  getDate(type = 'seconds', nextYear = 0) {
                      let d = new Date();
                      let year = d.getFullYear(), //当前年
                          month = d.getMonth() + 1, //当前月
                          day = d.getDate(), //当天
                          hours = d.getHours(), //当小时
                          minutes = d.getMinutes(), //当分钟
                          seconds = d.getSeconds(), //当秒
                          normal, //Y年M月D日 T:M:S
                          special, //Y-M-D T:M:S
                          end; //几年后的时间;
                      JSON.stringify(month).length == 1 ? month = `${'0' + month}` : month = month
                      JSON.stringify(hours).length == 1 ? hours = `${'0' + hours}` : hours = hours
                      JSON.stringify(minutes).length == 1 ? minutes = `${'0' + minutes}` : minutes = minutes
                      JSON.stringify(seconds).length == 1 ? seconds = `${'0' + seconds}` : seconds = seconds
                      switch (type) {
                          case "year":
                              normal = `${year+'年'}`;
                              special = `${year}`;
                              end = utils.getAfterNYear(special, nextYear);
                              break
                          case "month":
                              normal = `${year+'年'+month+'月'}`;
                              special = `${year+'-'+month}`;
                              end = utils.getAfterNYear(special, nextYear, 2, type);
                              break
                          case "day":
                              normal = `${year+'年'+month+'月'+day+'日'}`;
                              special = `${year+'-'+month+'-'+day}`;
                              end = utils.getAfterNYear(special, nextYear, 2, type);;
                              break
                          case "hours":
                              normal = `${year+'年'+month+'月'+day+'日'+' '+hours}`;
                              special = `${year+'-'+month+'-'+day+' '+hours}`;
                              end = utils.getAfterNYear(special, nextYear, 2, type);;
                              break
                          case "minutes":
                              normal = `${year+'年'+month+'月'+day+'日'+' '+hours+':'+minutes}`;
                              special = `${year+'-'+month+'-'+day+' '+hours+':'+minutes}`;
                              end = utils.getAfterNYear(special, nextYear, 2, type);;
                              break;
                          default:
                              normal = `${year+'年'+month+'月'+day+'日'+' '+hours+':'+minutes+':'+seconds}`;
                              special = `${year}-${month}-${day}-${hours}:${minutes}:${seconds}`;
                              end = utils.getAfterNYear(special, nextYear, 2, type);;
                              return;
                      }
                      return {
                          normal,
                          special,
                          end
                      };
                  },
                  //计算N年后,YYYYMMDD，startdate：为开始时间，格式YYYYMMDD，nextYear：为间隔年，1为一年，以此类推
                  getAfterNYear(startdate, nextYear = 1, nextMonth = 2, type = "seconds") {
                      console.log(type, 'type');
                      nextYear = nextMonth > 12 ? nextYear + Math.ceil(nextMonth / 12) : nextYear
                      let expriedYear = parseInt(startdate.substring(0, 4)) + nextYear;
                      let expriedMonth = parseInt(startdate.substring(5, 7)) + nextMonth
                      let expriedDay = startdate.substring(8);
                      let dateValue;
                      //考虑二月份场景，若N年后的二月份日期大于该年的二月份的最后一天，则取该年二月份最后一天
                      if (expriedMonth == '02' || expriedMonth == 2) {
                          let monthEndDate = new Date(expriedYear, expriedMonth, 0).getDate();
                          if (parseInt(expriedDay) > monthEndDate) { //为月底时间
                              //取两年后的二月份最后一天
                              expriedDay = monthEndDate;
                          }
                      }
                      if (expriedMonth >= 22) {
                          expriedMonth = `${expriedMonth - 12}`;
                      } else if (expriedMonth >= 13) {
                          expriedYear += Math.ceil((expriedMonth - 12) / 12);
                          expriedMonth = `${'0'+JSON.stringify(expriedMonth - 12)}`;
                      }
                      switch (type) {
                          case "year":
                              dateValue = expriedYear
                          case "month":
                              dateValue = expriedYear + '-' + expriedMonth;
                          default:
                              dateValue = expriedYear + '-' + expriedMonth + '-' + expriedDay;
                      }
                      return dateValue;
                  },
                  // 获取几个月后的时间
                  getAfterMonth(month) {
                      let time = new Date();
                      time.setDate(time.getDate()); //获取Day天后的日期 
                      let y = time.getFullYear();
                      let getMonth = time.getMonth();
                      let m;
                      if (getMonth + month + 1 > 12) {
                          y = y + 1;
                          m = getMonth + month - 11; //获取当前月份的日期 d
                      } else {
                          m = getMonth + month + 1; //获取当前月份的日期 d
                      }
                      var d = time.getDate();
                      return y + "-" + m + "-" + d;
                  },
              },
              /* 缓存相关 */
              cacheFuncs: {
                  // 存储setSessionItem ,name为存储的自定义昵称，content为要存储的数据
                  setSessionItem(name, content) {
                      if (!name) return;
                      if (typeof content !== 'string') {
                          content = JSON.stringify(content);
                      }
                      window.sessionStorage.setItem(name, content);
                  },
                  // 读取浏览器的setSessionItem
                  getSessionItem(name) {
                      if (!name) return;
                      return window.sessionStorage.getItem(name);
                  },
                  // 删除浏览器的setSessionItem
                  removeSessionItem(name) {
                      if (!name) return;
                      window.sessionStorage.removeItem(name);
                  },
                  // 存储localStorageItem ,name为存储的自定义昵称，content为要存储的数据
                  setLocalStorageItem(key, value, days) {
                      // 过期设置
                      if (!value) {
                          window.localStorage.removeItem(key)
                      } else {
                          var Days = days || 1; // 默认保留1天
                          var exp = new Date();
                          localStorage[key] = JSON.stringify({
                              value,
                              expires: exp.getTime() + Days * 24 * 60 * 60 * 1000
                          })
                      }
                  },
                  // 读取浏览器的LocalStorageItem
                  getLocalStorageItem(name) {
                      if (!name) return;
                      return window.localStorage.getItem(name);
                  },
                  // 删除浏览器的LocalStorageItem
                  removeSessionItem(name) {
                      if (!name) return;
                      window.LocalStorage.removeItem(name);
                  },
              },
              /* 获取url参数相关 */
              linkFuncs: {

              },
              /* 其他方法 */
              casualFuncs: {
                  //判断终端机型
                  pcOrMobile(u) {
                      let browser = { //移动终端浏览器版本信息
                          trident: u.indexOf('Trident') > -1, //IE内核
                          presto: u.indexOf('Presto') > -1, //opera内核
                          webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
                          gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
                          mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                          ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
                          android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
                          iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
                          iPad: u.indexOf('iPad') > -1, //是否iPad
                          webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
                      };
                      if (browser.mobile || browser.ios || browser.android || browser.iPhone || browser.iPad) {
                          return 'mobile'
                      } else {
                          return 'pc'
                      }
                  },
                  // h5页面刷新
                  webReload() {
                      window.location.reload();
                  },
                  // 深拷贝
                  deepClone(obj) {
                      let _obj = JSON.stringify(obj),
                          objClone = JSON.parse(_obj);
                      return objClone;
                  },
                  // 去除字符串中所有空格
                  trim(str) {
                      return str.replace(/\s/g, "");
                  },
              }
          }

      export default utils